FROM alpine:latest
COPY loki /etc/telemetry/loki
COPY tempo /etc/telemetry/tempo
COPY grafana-provisioning /etc/telemetry/grafana/provisioning
COPY prometheus /etc/telemetry/prometheus
COPY entry.sh /

# By default we are expecting .gen files to produce valid YAML
# files when executed. This is typically in order to insert
# secrets and other data that mustn't appear in GIT at runtime
# into the final config files, but can be used for any kind of
# shell-(env-var)-based runtime templating.
CMD /entry.sh
