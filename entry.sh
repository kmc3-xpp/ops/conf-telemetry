#!/bin/sh

echo "Generating config files in /etc/telemetry"
find /etc/telemetry -name *.gen -exec sh -c '{} > $(dirname {})/$(basename {} .gen) && rm {}' \;

# This is sometimes necessary so that the container stays around.
# It's for better interplay with systemd and podman auto-update.
if [ "$CONFIG_PERSIST" != "no" ]; then
    echo "Persisting forever."
    while true; do
        sleep 1d
    done
fi

