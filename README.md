Conf-Telemetry
==============

This repostitory holds static configuration & provisioning data for the
KMC3 telemetry collection backend & frontend. It consists of Grafana Dashboard
as the primary frontend, and Grafana Loki and Tempo for logs/traces, as
well as Prometheus as a metrics backend (so far).

All files that end on `.gen` and are executable are treated as shell
configuration templates (generators) that, when executed with `/bin/sh`,
produce a vaild configuration file of the same name just without the `.gen`
extension.
